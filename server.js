const express = require('express'),
      app = express(),
      port = process.env.PORT || 3000,
      mongoose = require('mongoose'),
      LicenciaProducto = require('./api/models'), //created model loading here
      bodyParser = require('body-parser'),
      routes = require('./api/routes'),
      cors = require('cors')
  
mongoose.Promise = global.Promise

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true }) 

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(cors())

routes(app)

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' no se ha encontrado el recurso que esta buscando'})
})

app.listen(port)

console.log('Licencia Productos RESTful API server started on: ' + port)