const LicenciaControlador = require('../controllers/licencia-producto')

module.exports = app => {

  app.route('/licencias')
    .get(LicenciaControlador.getAll)
    .post(LicenciaControlador.create)


  app.route('/licencias/:id')
    .get(LicenciaControlador.getById)
    .put(LicenciaControlador.update)
    .delete(LicenciaControlador.delete)

  app.route('/validarProducto/:id')
    .get(LicenciaControlador.isValidProduct)
}