const mongoose = require('mongoose')

const Schema = mongoose.Schema

const LicenciaProductoSchema = new Schema({
  titulo: {
    type: String,
  },
  descripcion: {
    type: String
  },
  fechaVencimiento: {
    type: Date
  },
  valido: {
    type: Number,
    enum: [1, 0, 2],
    default: 1
  }
})

module.exports = mongoose.model('LicenciaProductos', LicenciaProductoSchema)
