const mongoose = require('mongoose')
const authentication = require('../../utilities/Authentication')

const LicenciaProducto = mongoose.model('LicenciaProductos')

exports.getAll = (req, res) => {
  LicenciaProducto.find(
    {}, 
    (err, task) => {
      if (err)
        res.send(err)

      res.json(task)
    })
}

exports.getById = (req, res) => {
  LicenciaProducto.findById(
    req.params.id, 
    (err, licencia) => {
      if (err)
        res.send(err)

      res.json(licencia)
    })
}

exports.create = (req, res) => {
  if (authentication(req.body.token)) {
    const nuevaLicencia = new LicenciaProducto(req.body)

    nuevaLicencia.save(
      (err, licencia) => {
        if (err)
          res.send(err);

        licencia.resultado = 'OK'
        res.json(licencia)
      })
  } else {
    res.sendStatus(401)
  }
}

exports.update = (req, res) => {
  if (authentication(req.body.token)) {
    LicenciaProducto.findOneAndUpdate(
      { _id: req.params.id }, 
      req.body, 
      { new: true }, 
      (err, licencia) => {
        if (err)
          res.send(err)

        licencia.resultado = 'OK'
        res.json(licencia)
      })
  } else {
    res.sendStatus(401)
  }
}

exports.delete = (req, res) => {
  if (authentication(req.body.token)) {
    LicenciaProducto.remove(
      { _id: req.params.id },
      (err, licencia) => {
        if (err)
          res.send(err)

        licencia.resultado = 'OK'
        res.json(licencia)
      })
  } else {
    res.sendStatus(401)
  }
}

exports.isValidProduct = (req, res) => {
  LicenciaProducto.findById(
    req.params.id, 
    (err, licencia) => {
      if (err)
        res.send(err)

      /*if (licencia.valido === 2) {
        const fecha = new Date().setHours(0, 0, 0, 0)
        licencia.fechaVencimiento = licencia.fechaVencimiento.setHours(0, 0, 0, 0)

        licencia.valido = fecha > licencia.fechaVencimiento ? 0 : 1
      }*/
      
      const { descripcion, valido, fechaVencimiento } = licencia

      res.json({ 
        descripcion,
        fechaVencimiento,
        valido 
      })
    })
}
